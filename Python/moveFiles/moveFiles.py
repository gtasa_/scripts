import os
import shutil
download_folder = "C:/Users/GTASA/Downloads/"

def moveFile(file, directory):
    print(f'Moviendo: {file} a {download_folder + directory}')
    shutil.move(file, download_folder + directory) # Moves the file to directory


def handleFile(file, directory):

    directory_exists = os.path.isdir(download_folder + directory) # True // False

    if directory_exists:
        # move file
        moveFile(file, directory)
    else:
        os.makedirs(download_folder + directory) # create directory if it doesn't exists
        moveFile(file, directory)

def extensionAllowed(extension, extensionsAllowed):
    # extension is a string. Can be: '.txt' '.exe' '.png'. etc...
    # extensionsAllowed is a list. Contains the allowed extensions. Ex: ['.png', '.jpeg']

    if extension in extensionsAllowed:
        return True
    else:
        return False

list_of_files_to_move = [
    {
        "target_folder_name": "Executables",
        "allowed_files_extension": ['.exe', '.msi']
    },
    {
        "target_folder_name": "Rars", 
        "allowed_files_extension": ['.rar', '.zip']
    },
    {
        "target_folder_name": "Torrents", 
        "allowed_files_extension": ['.torrent']
    },
    {
        "target_folder_name": "TXT", 
        "allowed_files_extension": ['.txt']
    },
    {
        "target_folder_name": "Images", 
        "allowed_files_extension": ['.png', '.jpg', '.jpeg']
    },
    {
        "target_folder_name": "PDFs", 
        "allowed_files_extension": ['.pdf', '.epub']
    },
    {
        "target_folder_name": "MP4", 
        "allowed_files_extension": ['.mp4']
    },
    {
        "target_folder_name": "MP3", 
        "allowed_files_extension": ['.mp3']
    },
    {
        "target_folder_name": "WAV", 
        "allowed_files_extension": ['.wav']
    },
    {
        "target_folder_name": "Gifs", 
        "allowed_files_extension": ['.gif']
    },
    {
        "target_folder_name": "ExcelFiles", 
        "allowed_files_extension": ['.xlsx']
    },
    {
        "target_folder_name": "PSD", 
        "allowed_files_extension": ['.psd']
    },
]

if __name__ == '__main__':
    for foundFiles in os.listdir(download_folder):
        file_name, file_extension = os.path.splitext(download_folder + foundFiles)
        full_file = file_name + file_extension

        for files in list_of_files_to_move:
            allowed_extensions = files['allowed_files_extension']
            directory_to_move = files['target_folder_name']

            if extensionAllowed(file_extension, allowed_extensions):
                handleFile(full_file, directory_to_move)