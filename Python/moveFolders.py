import os
import shutil

download_folder = "C:/Users/GTASA/Downloads/"

dirs = [d for d in os.listdir(download_folder) if os.path.isdir(os.path.join(download_folder, d))]

for dir in dirs:
    path = download_folder + dir
    if not (dir in ["Executables", "Rars", "Torrents", "TXT", "Images", "PDFs", "MP4", "MP3", "WAV", "Gifs", "ExcelFiles", "PSD", "Folders"]):
        try:
            shutil.move(path, download_folder + 'Folders')
        except(Exception):
            print(Exception)