// ==UserScript==
// @name         OF - Welcome
// @namespace    https://onlyfans.com/my/chats/*
// @version      0.4
// @description  try to take over the world!
// @author       Josmanuel Sandrea
// @match        https://onlyfans.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// @downloadURL  https://gitlab.com/gtasa_/scripts/-/blob/master/OF%20Scripts/AutomateNewSubsHi.js
// @updateURL  https://gitlab.com/gtasa_/scripts/-/blob/master/OF%20Scripts/AutomateNewSubsHi.js
// ==/UserScript==

(function () {
    'use strict';
    let hi_mode = true


    document.addEventListener('keydown', (e)=>{

        if (e.ctrlKey && e.key == 'q'){
            if (hi_mode == true) {
                hi_mode = false
                alert('hi mode is 🔴🔴 now')
            }
            else if (hi_mode == false) {
                hi_mode = true
                alert('hi mode is 🟢🟢 now')
            }
        }
    })

    // test

    setInterval(automateHi, 2000);
    setInterval(lookForChatButton, 2000);

    function randomHiMessage(username, number) {
        if (username == 'u') return `Hello baby what's your name?`
        if (number == 0) return `Hey ${username}, how're you? How's your day going? 💕`
        if (number == 1) return `Hello ${username}! Where are you from?`
        if (number == 2) return `Hi ${username}, what are your plans for today?`
    }

    function automateHi() {
        const RandomNumber = Math.floor(Math.random() * 3)
        let textarea = document.getElementById('new_post_text_input')

        if (textarea) {
            textarea.addEventListener('keypress', (e) => {
                if (e.key == '-') {
                    let username = document.querySelector('.g-user-name .g-user-realname__wrapper .g-user-name').innerText
                    username = username.replace(/[0-9]/g, '').split(' ')
                    textarea.value = randomHiMessage(username[0], RandomNumber)
                }

                if (e.key == 'm' && hi_mode == true){
                    let username = document.querySelector('.g-user-name .g-user-realname__wrapper .g-user-name').innerText
                    textarea.value = `Hello ${username}! ❤❤. How's your day going?`
                }
            })
        }
    }


    function lookForChatButton() {
        let chatButton = document.querySelector('.g-btn.m-rounded.m-rounded.m-icon')

        if (chatButton) {
            chatButton.click()
        }
    }

    lookForChatButton()
})();