// ==UserScript==
// @name         Onlyfans automate summary
// @namespace    https://onlyfans.com/my/statements/earnings
// @version      0.1
// @description  Makes automatic summary on statements
// @author       Josmanuel Sandrea
// @match        https://onlyfans.com/my/statements/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function () {
    'use strict';
    const todoslosriales = []
    const tablasValidas = []
    const reducer = (accumulator, curr) => accumulator + curr
    const date = new Date()
    const dayask = date.getDate()
    const nextDayAsk = dayask + 1

    function Int(string) {
        return parseInt(string)
    }

    function Float(string) {
        return parseFloat(string)
    }

    function dollarToNumber(string) {
        return Float(string.innerText.replace('$', ''))
    }

    function findTableDay(element) {
        let day_string = element.parentNode.childNodes[0].childNodes[0].innerText

        if (day_string.indexOf(dayask) !== -1) {
            return Int(dayask)
        } else {
            return 0
        }
    }

    const MainInterval = setInterval(() => {
        const tablepayment = document.querySelectorAll('tr td.b-table__date span.b-table__date__time')
        tablepayment.forEach(x => {
            let hour = x.innerText.split(' ').join().replace(',', ':').split(':')

            let table_data = {
                "hour": Int(hour[0]),
                "minutes": Int(hour[1]),
                "time": hour[2],
                "money": dollarToNumber(x.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0]),
                "day": findTableDay(x),
                "element": x
            }

            if (
                table_data.hour == 8 && table_data.minutes > 5 && table_data.time == 'pm' ||
                table_data.hour >= 8 && table_data.time == 'pm' ||
                table_data.hour == 12 && table_data.time == 'am' ||
                table_data.hour >= 1 && table_data.hour <= 4 && table_data.time == 'am'
            ) {
                if (table_data.hour == 4 && table_data.minutes > 5) {
                    if (table_data.day == Int(nextDayAsk)) {
                        return
                    } else if (table_data.day !== Int(dayask)) {
                        return
                    }
                } else {
                    todoslosriales.push(table_data.money)
                    tablasValidas.push(table_data)
                    clearInterval(MainInterval)
                }
            }
        })
    }, 3000);

    document.addEventListener('keypress', (e) => {
        if (e.key == '+') {
            try {
                alert(todoslosriales.reduce(reducer) + '$')
                tablasValidas.forEach(x => {
                    x.element.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0].style.backgroundColor = '#cc3300'
                    x.element.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0].style.padding = '1em'
                })
            } catch (e) {
                alert('No hubo suerte por estos laos')
            }
        }
    })

})();