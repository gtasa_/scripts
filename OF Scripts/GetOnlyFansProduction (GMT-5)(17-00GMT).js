// ==UserScript==
// @name         OF - Statements
// @namespace    https://onlyfans.com/my/statements/earnings
// @version      0.2
// @description  Sum all the numbers of your shift!
// @author       Josmanuel Sandrea
// @match        https://onlyfans.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function(){
    'use strict';
    const reducer = (accumulator, curr) => accumulator + curr
    const currentDay = new Date().getDate()
    
    
    function main(){
        let First6HoursMoney = [0] // array with the first 6 hours money
        let Last2HoursMoney = [0] // array with the last 2 hours money
        
        let tablesWithInfo = [] // all the tables with structured objects

        let allTables = document.querySelectorAll('.b-table__date')

        allTables.forEach(table => {
        
            // look for date table
            let date = table.childNodes[0].childNodes[0].innerText
            date = date.replace(',', '').split(' ')
        
            // look for hour data
            let hour_data = table.childNodes[1].childNodes[0].innerText
            hour_data = hour_data.replace(':', ' ').split(' ')
        
            // look for the table money
            let money = table.parentNode.childNodes[3].childNodes[0].innerText.replace('$', '')
        
            // create object structure
            let table_info = {
                month: date[0],
                day: parseInt(date[1]),
                year: date[2],
                hour: parseInt(hour_data[0]),
                minutes: parseInt(hour_data[1]),
                meridiem: hour_data[2],
                money: parseFloat(money),
                tableElement: table.parentNode.childNodes[3].childNodes[0],
            }
            
            // send the object to the array
            tablesWithInfo.push(table_info)
        })
        
        tablesWithInfo.forEach(table => {
            if (table.day == currentDay){
                if(
                    table.hour == 12 && table.minutes > 5 && table.meridiem == 'pm' ||
                    table.hour >= 1 && table.hour <= 5 && table.meridiem == 'pm'
                ){
                    console.log(table.tableElement.style.backgroundColor)
                    table.tableElement.style.backgroundColor = '#87FF65'
                    table.tableElement.style.padding = '.5em'
                    table.tableElement.style.color = 'black'
                    First6HoursMoney.push(table.money)
                } else if (table.hour >= 6 && table.hour <= 8 && table.meridiem == 'pm'){
                    if (table.hour == 8 && table.minutes > 5) return
                    table.tableElement.style.backgroundColor = '#BF1A2F'
                    table.tableElement.style.padding = '.5em'
                    Last2HoursMoney.push(table.money)
                }
                // if (
                //     table.hour == 12 && table.minutes > 5 && table.meridiem == 'pm' ||
                //     table.hour >= 1 && table.hour <= 8 && table.meridiem == 'pm'
                // ){
                //     if (table.hour == 8 && table.minutes > 5) return
                //     table.tableElement.style.backgroundColor = '#f843E1'
                //     table.tableElement.style.padding = '.5em'
                //     allMoney.push(table.money)
                // }
            }
        })

        let totalMoney = First6HoursMoney.concat(Last2HoursMoney)
        
        alert(`
            Primeras 6 horas: ${First6HoursMoney.reduce(reducer) + '$'}\n
            Ultimas 2 horas: ${Last2HoursMoney.reduce(reducer) + '$'}\n
            Total: ${totalMoney.reduce(reducer) + '$'}\n
        `)
    }
    
    document.addEventListener('keypress', ({ key })=>{
        if (key == '0') main()
    })
})();